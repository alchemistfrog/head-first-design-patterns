package headfirst.behavioral.decorator.beverage.decorator;

import headfirst.behavioral.decorator.beverage.Beverage;

public class Whip extends CondimentDecorator {
	
	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return "whip";
	}

	@Override
	public double cost() {
		return this.beverage.cost() + .10;
	}
	
}
