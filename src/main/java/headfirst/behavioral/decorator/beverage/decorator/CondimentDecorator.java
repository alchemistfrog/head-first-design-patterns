package headfirst.behavioral.decorator.beverage.decorator;

import headfirst.behavioral.decorator.beverage.Beverage;
import headfirst.behavioral.decorator.beverage.Size;

public abstract class CondimentDecorator extends Beverage {

	public Beverage beverage;
	public abstract String getDescription();
	
	public Size getSize() {
		return this.beverage.getSize();
	}

}
