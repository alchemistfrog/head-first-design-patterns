package headfirst.behavioral.decorator.beverage.decorator;

import headfirst.behavioral.decorator.beverage.Beverage;
import headfirst.behavioral.decorator.beverage.Size;

public class Mocha extends CondimentDecorator {
	
	public Mocha(Beverage beverage) {
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return "mocha";
	}

	@Override
	public double cost() {
		if(beverage.getSize() == Size.TALL) {
			return this.beverage.cost() + .10;
		} else if (beverage.getSize() == Size.GRANDE) {
			return this.beverage.cost() + .15;
		} else if (this.beverage.getSize() == Size.VENTI) {
			return this.beverage.cost() + .20;
		}
		
		return this.beverage.cost();
	}

}
