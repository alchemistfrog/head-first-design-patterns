package headfirst.behavioral.decorator.beverage;

public enum Size {
	TALL, GRANDE, VENTI;
}
