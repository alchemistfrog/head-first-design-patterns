package headfirst.behavioral.decorator.beverage;

public abstract class Beverage {
	
	private String description = "Unknow";
	private Size size; 
	
	public String getDescription() {
		return this.description;
	}
	
	public abstract double cost();

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}
	
}
