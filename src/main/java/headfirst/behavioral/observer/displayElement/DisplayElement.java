package headfirst.behavioral.observer.displayElement;

public interface DisplayElement {

	public void display();
	
}
