package headfirst.behavioral.observer.subscriber;

import headfirst.behavioral.observer.valueObject.Measure;

public interface ObserverInterface {
	
	public void update(Measure measure);

	public Measure getMeasure();
}
