package headfirst.behavioral.observer.subscriber;

import java.util.ArrayList;
import java.util.List;

import headfirst.behavioral.observer.displayElement.DisplayElement;
import headfirst.behavioral.observer.subject.SubjectInterface;
import headfirst.behavioral.observer.valueObject.Measure;

public class StatisticsDisplay implements DisplayElement, ObserverInterface {

	private SubjectInterface subject;
	private Measure measure;
	private List<Measure> measures;
	private double averageHumidity;
	private double averagePressure;
	private double averageTemperature;
	
	public StatisticsDisplay(SubjectInterface subject) {
		this.subject = subject;
		this.subject.registerObserver(this);
		this.measures = new ArrayList<Measure>();
		this.averageHumidity = 0;
		this.averagePressure = 0;
		this.averageTemperature = 0;
	}
	
	@Override
	public void update(Measure measure) {
		this.measure = measure;
		measures.add(measure);
		this.updateAverageValues();
		display();
	}

	@Override
	public void display() {
		System.out.println("Average humidity: " + this.averageHumidity);
		System.out.println("Average pressure: " + this.averagePressure);
		System.out.println("Average temperature: " + this.averageTemperature);
	}
	
	private void updateAverageValues() {
		double h = 0;
		double p = 0;
		double t = 0;
		double measuresNb = this.measures.size();
		
		for (Measure m : measures) {
			h += m.getHumidity();
			p += m.getPressure();
			t = m.getTemperature();
		}
		
		this.averageHumidity = h / measuresNb;
		this.averagePressure = p / measuresNb;
		this.averageTemperature = t / measuresNb;
	}

	public Measure getMeasure() {
		return this.measure;
	}

}
