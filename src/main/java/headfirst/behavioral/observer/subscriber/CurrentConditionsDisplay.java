package headfirst.behavioral.observer.subscriber;

import headfirst.behavioral.observer.displayElement.DisplayElement;
import headfirst.behavioral.observer.subject.SubjectInterface;
import headfirst.behavioral.observer.valueObject.Measure;

public class CurrentConditionsDisplay implements ObserverInterface, DisplayElement {

	private Measure measure;
	private SubjectInterface subject;
	
	public CurrentConditionsDisplay(SubjectInterface subject) {
		this.subject = subject;
		subject.registerObserver(this);
	}
	
	@Override
	public void display() {
		System.out.println("Humidity: " + measure.getHumidity());
		System.out.println("Pressure: " + measure.getPressure());
		System.out.println("Temperature: " + measure.getTemperature());
	}

	@Override
	public void update(Measure measure) {
		this.measure = measure;
		display();
	}

	public Measure getMeasure() {
		return this.measure;
	}

}
