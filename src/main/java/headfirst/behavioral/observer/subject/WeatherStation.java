package headfirst.behavioral.observer.subject;

import java.util.ArrayList;
import java.util.List;

import headfirst.behavioral.observer.subscriber.ObserverInterface;
import headfirst.behavioral.observer.valueObject.Measure;

public class WeatherStation implements SubjectInterface {

	private List<ObserverInterface> observers;
	private Measure measure;
	
	public WeatherStation() {
		this.observers = new ArrayList<ObserverInterface>();
	}
	
	@Override
	public void registerObserver(ObserverInterface subscriber) {
		this.observers.add(subscriber);
	}

	@Override
	public void deleteObserver(SubjectInterface subject) {
		int idx = this.observers.indexOf(subject);

		if(idx >= 0) {
			this.observers.remove(idx);
		}
	}
	
	@Override
	public void notifyObservers() {
		for (ObserverInterface observer : this.observers) {
			observer.update(this.measure);
		}
	}
	
	public void measurementsChanged() {
		this.notifyObservers();
	}
	
	public void setMeasure(Measure measure) {
		this.measure = measure;
		this.notifyObservers();
	}

}
