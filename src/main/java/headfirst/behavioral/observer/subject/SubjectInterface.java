package headfirst.behavioral.observer.subject;

import headfirst.behavioral.observer.subscriber.ObserverInterface;
import headfirst.behavioral.observer.valueObject.Measure;

public interface SubjectInterface {
	
	public void registerObserver(ObserverInterface subscriber);
	
	public void deleteObserver(SubjectInterface subjectInterface);
	
	public void notifyObservers();

	public void setMeasure(Measure measure);
}
