package headfirst.behavioral.observer.valueObject;

public class Measure {

	private double temperature;

	private double humidity;

	private double pressure;

	private Measure() {
	}
	
	public static Measure buildFromData(double temperature, double humidity, double pressure) {
		Measure m = new Measure();
		
		m.temperature = temperature;
		m.humidity = humidity; 
		m.pressure = pressure;
		
		return m;
	}
	
	public double getTemperature() {
		return temperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public double getPressure() {
		return pressure;
	}

}
