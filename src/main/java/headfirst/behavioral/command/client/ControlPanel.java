package headfirst.behavioral.command.client;

import headfirst.behavioral.command.command.LightOffCommand;
import headfirst.behavioral.command.command.LightOnCommand;
import headfirst.behavioral.command.invoker.LightRemoteControl;
import headfirst.behavioral.command.receiver.Light;

import java.util.Locale;
import java.util.Scanner;

public class ControlPanel {

    private static LightRemoteControl remoteControl = new LightRemoteControl();
    private static Light light = new Light();
    private static LightOnCommand lightOnCommand = new LightOnCommand(light);
    private static LightOffCommand lightOffCommand = new LightOffCommand(light);

    public static void main(String[] args) {

        while(true) {
            System.out.println("Type ON or OFF to switch the light on or off.");
            Scanner sc = new Scanner(System.in);
            String button = sc.nextLine().toUpperCase(Locale.ROOT);

            if (button.equals("ON")) {
                turnOnLight();
            } else if (button.equals("OFF")) {
                turnOffLight();
            } else {
                System.out.println("You need to type ON or OFF");
            }

            if (light.getLightState() == true) {
                System.out.println("The light is on.");
            } else {
                System.out.println("The light is off.");
            }

        }
    }

    private static void turnOnLight() {
        remoteControl.setCommand(lightOnCommand);
        remoteControl.buttonWasPressed();
    }

    public static void turnOffLight() {
        remoteControl.setCommand(lightOffCommand);
        remoteControl.buttonWasPressed();
    }

}
