package headfirst.behavioral.command.command;

public interface Command {

    public void execute();

}
