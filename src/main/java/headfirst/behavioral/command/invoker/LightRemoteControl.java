package headfirst.behavioral.command.invoker;

import headfirst.behavioral.command.command.Command;

public class LightRemoteControl {

    private Command slot;

    public LightRemoteControl(){}

    public void setCommand(Command command) {
        this.slot = command;
    }

    public void buttonWasPressed() {
        slot.execute();
    }
}
