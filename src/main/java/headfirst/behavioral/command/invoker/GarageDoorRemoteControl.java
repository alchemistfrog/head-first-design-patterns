package headfirst.behavioral.command.invoker;

import headfirst.behavioral.command.command.Command;

public class GarageDoorRemoteControl {

    Command slot;

    public GarageDoorRemoteControl(Command command) {
        this.slot = command;
    }

    public void buttonWasPressed() {
        this.slot.execute();
    }

}
