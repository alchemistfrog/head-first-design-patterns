package headfirst.behavioral.command.receiver;

import headfirst.behavioral.command.command.Command;

public class GarageDoor {

    private boolean doorState;
    private boolean lightState;

    public GarageDoor() {
        this.doorState = false;
        this.lightState = false;
    }

    public void up() {
        if (doorState) {
            System.out.println("Door already opened");
            return;
        }

        lightOn();
        System.out.println("The door is opening...");
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            System.out.println("Operation aborted");
        } finally {
            lightOff();
        }

        lightOff();
        doorState = true;
        System.out.println("The door is open");
    }

    public void down() {
        if(!doorState) {
            System.out.println("Door already closed");
            return;
        }

        lightOn();
        System.out.println("The door is closing...");
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            System.out.println("Operation aborted");
        } finally {
            lightOff();
        }

        lightOff();
        doorState = false;
        System.out.println("The door is closed");
    }

    public void stop() {
        System.out.println("Operation aborted");
        lightOff();
    }

    private void lightOn() {
        if(!this.lightState) {
            System.out.println("The light is on");
            this.lightState = true;
        }
    }

    private void lightOff() {
        if(this.lightState) {
            System.out.println("The light is off");
            this.lightState = false;
        }
    }

    public boolean getDoorState() {
        return this.doorState;
    }

    public boolean getLightState() {
        return this.lightState;
    }
}
