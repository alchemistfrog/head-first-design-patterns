package headfirst.behavioral.command.receiver;

public class Light {

    private boolean lightState;

    public Light() {
        lightState = false;
    }

    public void on() {
        if (!this.lightState) {
            this.lightState = true;
        }
    }

    public void off() {
        if (this.lightState) {
            this.lightState = false;
        }
    }

    public boolean getLightState() {
        return this.lightState;
    }
}
