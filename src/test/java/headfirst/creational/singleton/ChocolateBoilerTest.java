package headfirst.creational.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChocolateBoilerTest {

    @Test
    public void testChocolateBoilerSingletonCreateUniqueInstance() {
        ChocolateBoiler boiler = ChocolateBoiler.getInstance();
        ChocolateBoiler boiler2 = ChocolateBoiler.getInstance();

        Assertions.assertEquals(boiler, boiler2);
    }

}
