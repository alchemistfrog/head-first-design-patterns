package headfirst.behavioral.command;

import headfirst.behavioral.command.command.LightOffCommand;
import headfirst.behavioral.command.command.LightOnCommand;
import headfirst.behavioral.command.invoker.LightRemoteControl;
import headfirst.behavioral.command.receiver.Light;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CommandTest {

    private static LightRemoteControl remoteControl;
    private static Light light;
    private static LightOnCommand switchOn;
    private static LightOffCommand switchOff;

    @BeforeAll
    public static void initialize() {
        remoteControl = new LightRemoteControl();
        light = new Light();
        switchOn = new LightOnCommand(light);
        switchOff = new LightOffCommand(light);
    }

    @Test
    public void testLightIsOffByDefault() {
        Light light = new Light();

        Assertions.assertFalse(light.getLightState());
    }

    @Test
    public void testLightOnCommandSwitchTheLightOn() {
        remoteControl.setCommand(switchOn);
        remoteControl.buttonWasPressed();

        Assertions.assertTrue(light.getLightState());
    }

    @Test
    public void testLightOffCommandSwitchTheLightOff() {
      remoteControl.setCommand(switchOff);
      remoteControl.buttonWasPressed();

      Assertions.assertFalse(light.getLightState());
    }

}
