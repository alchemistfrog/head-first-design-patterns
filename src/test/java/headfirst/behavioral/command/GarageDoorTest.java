package headfirst.behavioral.command;

import headfirst.behavioral.command.command.GarageDoorOpenCommand;
import headfirst.behavioral.command.invoker.GarageDoorRemoteControl;
import headfirst.behavioral.command.receiver.GarageDoor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class GarageDoorTest {

    private static GarageDoor garageDoor;
    private static GarageDoorOpenCommand openCommand;

    @BeforeAll
    public static void initialize() {
        garageDoor = new GarageDoor();
        openCommand = new GarageDoorOpenCommand(garageDoor);
    }

    @Test
    public void testGarageDoorIsClosedByDefault() {
        GarageDoor garageDoor = new GarageDoor();

        Assertions.assertFalse(garageDoor.getDoorState());
    }

    @Test
    public void testGarageDoorOpeningCommand() {
        GarageDoorRemoteControl remoteControl = new GarageDoorRemoteControl(openCommand);

        remoteControl.buttonWasPressed();

        Assertions.assertTrue(garageDoor.getDoorState());
    }

}
