package headfirst.behavioral.observer;

import headfirst.behavioral.observer.subject.SubjectInterface;
import headfirst.behavioral.observer.subject.WeatherStation;
import headfirst.behavioral.observer.subscriber.CurrentConditionsDisplay;
import headfirst.behavioral.observer.subscriber.ObserverInterface;
import headfirst.behavioral.observer.valueObject.Measure;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ObserverPatternTest {

    @Test
    public void testObserver() {

        SubjectInterface weatherStation = new WeatherStation();
        ObserverInterface currentConditionDisplay = new CurrentConditionsDisplay(weatherStation);
        Measure measure = Measure.buildFromData(12.0, 1.0, .2);

        weatherStation.setMeasure(measure);

        Assertions.assertEquals(measure.getTemperature(), currentConditionDisplay.getMeasure().getTemperature());
        Assertions.assertEquals(measure.getHumidity(), currentConditionDisplay.getMeasure().getHumidity());
        Assertions.assertEquals(measure.getPressure(), currentConditionDisplay.getMeasure().getPressure());
    }


}
