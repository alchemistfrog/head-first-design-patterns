package headfirst.behavioral.decorator.beverage;

import headfirst.behavioral.decorator.beverage.decorator.Mocha;
import headfirst.behavioral.decorator.beverage.decorator.Whip;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BeverageTest {

    @Test
    public void testDecorateBeverageWithCondiment()
    {
        Beverage expresso = new Expresso();

        expresso.setSize(Size.GRANDE);
        expresso = new Mocha(expresso);
        expresso = new Whip(expresso);

        Assertions.assertEquals(2.24, expresso.cost());
    }

}
